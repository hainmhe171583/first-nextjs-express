var knex = require('../api/knexdb');



async function getListTable(user_id) {
    // var username = getUser(req, res);
    return knex('task')
        .where({ assigned_user_id: user_id })
        .join('table_task','task.table_task_Id' , '=', 'table_task.Id')
        .join('user', 'table_task.user_id','=','user.Id')
        .select('task.Id','task.table_task_id','task.title','task.description', 'task.status_id', 'user.username','table_task.title as task_table')
        // .then((row) => {
        //     res.send(row);
        // })
}






module.exports = getListTable;