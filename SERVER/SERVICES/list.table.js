var knex = require('../api/knexdb');

// function getUser(req, res) {
//     var tokenString = JSON.stringify(req.headers['tokenstring']);
//     tokenString = tokenString.substring(1, tokenString.length-1);

//     try {
//         const username = jwt.verify(tokenString, process.env.ACCESS_TOKEN_SECRET).username;
//         return username
//     } catch (err) {
//         console.log(err);
//         return -1;
//     }
// }

async function getListTable(username) {
    // var username = getUser(req, res);
    return knex('table_task')
        .where({ username: username })
        .join('user', 'user.Id', '=', 'table_task.user_id')
        .select('table_task.Id','table_task.title')
        // .then((row) => {
        //     res.send(row);
        // })
}





module.exports = getListTable;