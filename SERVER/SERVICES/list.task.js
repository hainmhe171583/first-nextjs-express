var knex = require('../api/knexdb');



async function getListtask(table_task_id) {
    // var username = getUser(req, res);
    // console.log(req.query.table_task_id);
    return knex('task')
        .where({ table_task_id: table_task_id})
        .join('table_task', 'table_task.Id', '=', 'task.table_task_id')
        .leftJoin('user','task.assigned_user_id','=','user.Id')
        .select('table_task.title as table_title','table_task.user_id','task.Id','task.title', 'task.description', 'task.status_id', 'user.username')
        // .then((row) => {
        //     //console.log(row)
        //     res.send(row);
        // })
}







module.exports = getListtask;