var knex = require('../api/knexdb');

async function newtask(table_task_id, title, description) {
    // let { table_task_id, title, description } = req.body;
    return knex.insert([
        {
            table_task_id: table_task_id,
            title: title,
            description: description,
            status_id: 1
        }
    ]).into('task')
    // .catch(function (err) {
    //     console.log(err);
    // });
    // res.send({
    //     authsuccess: true,
    //     Status: true
    // });
}


module.exports = newtask;