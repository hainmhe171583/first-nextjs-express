var knex = require('../api/knexdb');

async function getListTable(table_task_id) {
    // var username = getUser(req, res);
    // console.log(req.query.table_task_id);
    return knex('table_task')
        .where({ 'table_task.Id': table_task_id })
        .join('assign', 'assign.table_task_id', '=', 'table_task.Id')
        .join('user','assign.user_id','=','user.Id')
        .select('assign.user_id','user.username')
        // .then((row) => {
        //     res.send(row);
        // })
}





module.exports = getListTable;