var knex = require('../knexdb');
var express = require('express');
var app = express();
var cors = require('cors');
var bodyparser = require('body-parser');
var token = require('./token');
// var session = require('express-session');
var cookieParser = require('cookie-parser');

app.use(cors());
app.use(bodyparser.json());
// app.use(session({
//     secret: 'todolist',
//     resave: false,
//     saveUninitialized: true,
//     cookie: {
//         secure: false,
//         expires: 1000 * 60 * 60 * 24
//     }
// }))



function findUser(req,res, username, password) {
    knex('user').where({
        username: username
    }).select('Id','username', 'password').then(
        (row) => {
            // console.log(row);
            if (row.length != 0) {
                if (row[0].password == password){
                        //console.log(token(req, res));
                        let tokenString = token(req, res);
                        
                        var date = new Date();
                        date.setDate(date.getDate() + 1)
                        //console.log(date.toISOString().replace(/T/, ' ').replace(/\..+/, '') ); 
                        // res.cookie('tokenInfo',JSON.stringify({
                        //     tokenString : tokenString,
                        //     expires : date.toISOString().replace(/T/, ' ').replace(/\..+/, '')
                        // }));
                        
                        res.send({
                            authsuccess : true,
                            tokenString : tokenString,
                            expires : date.toISOString().replace(/T/, ' ').replace(/\..+/, ''),
                            username: username,
                            userId: row[0].Id
                        });
                        //console.log(token);
                        
                    }
                        //console.log("success!!")
                else
                res.send({
                    authsuccess : false,
                    message : "Wroong password"    
                });
                    //res.json("wroong password!!");
                    //console.log("wroong pass!!")
                }
            else {
                res.send({
                    authsuccess : false,
                    message : "not found user!!"    
                });
                //res.json("not found user!!");
                //console.log("not found user!!")
            }
        }

    );
}

app.post('/', (req, res) => {
    console.log("submited");

    //console.log(req.body);
    let { username, password } = req.body;
    //console.log(username);
    findUser(req, res, username, password);

    //res.json("submited from login form");
}
);


app.get('/', (req, res) =>
    res.send("/api/login"));

module.exports = app;