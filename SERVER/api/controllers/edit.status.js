var express = require('express');
var app = express();
var cors = require('cors');
var bodyparser = require('body-parser');
var editStatus = require("../../services/edit.status");

app.use(cors());
app.use(bodyparser.json());

// function editStatus(req, res) {

//     //  console.log(status);
//     knex('task')
//         .where('Id', '=', task_id)
//         .update({
//             status_id:status
//         })
// .catch(
//             function (err) {
//                 console.log(err)
//             });
//     res.send({
//         authsuccess: true,
//         Status: true
//     });
// }


app.post('/', async (req, res) => {
    console.log('submit your edition!');
    let { task_id, status } = req.body;
    await editStatus(task_id, status)
    .catch(
            function (err) {
                console.log(err);
                res.send({
                    authsuccess: true,
                    Status: false
                })
            });
    res.send({
        authsuccess: true,
        Status: true
    });

})


module.exports = app;