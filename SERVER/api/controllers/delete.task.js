var knex = require('../knexdb');
var express = require('express');
var app = express();
var cors = require('cors');
var bodyparser = require('body-parser');
var jwt = require("jsonwebtoken");
var deletetask = require("../../services/delete.task");

app.use(cors());
app.use(bodyparser.json());

// function deletetask(req, res) {
//     let { Id } = req.body;
//     //console.log(Id);

//     knex('task')
//         .where('Id',Id)
//         .del()
//         .catch(
//         function (err) {
//             console.log(err)
//         });

//     res.send({
//         authsuccess: true,
//         Status: true
//     });
// }

app.post('/', async  (req, res) => {
    console.log('submited for delete a task!!');
    let { Id } = req.body;
    await deletetask(Id)
        .catch(
            function (err) {
                console.log(err);
                return res.send({
                    authsuccess: true,
                    Status: false
                });
            });

    res.send({
        authsuccess: true,
        Status: true
    });
});

module.exports = app;