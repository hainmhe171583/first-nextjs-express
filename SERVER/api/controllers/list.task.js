var knex = require('../knexdb');
var express = require('express');
var app = express();
var cors = require('cors');
var bodyparser = require('body-parser');
var jwt = require("jsonwebtoken");
var getListtask = require("../../services/list.task");

app.use(cors());
app.use(bodyparser.json());


// function getUser(req, res) {
//     var tokenString = JSON.stringify(req.headers['tokenstring']);
//     tokenString = tokenString.substring(1, tokenString.length-1);

//     try {
//         const username = jwt.verify(tokenString, process.env.ACCESS_TOKEN_SECRET).username;
//         return username
//     } catch (err) {
//         console.log(err);
//         return -1;
//     }
// }

// function getListtask(req, res) {
//     console.log(req.query.table_task_id);
//     knex('task')
//         .where({ table_task_id:  req.query.table_task_id})
//         .join('table_task', 'table_task.Id', '=', 'task.table_task_id')
//         .leftJoin('user','task.assigned_user_id','=','user.Id')
//         .select('table_task.title as table_title','table_task.user_id','task.Id','task.title', 'task.description', 'task.status_id', 'user.username')
//         .then((row) => {
//             //console.log(row)
//             res.send(row);
//         })
// }





app.get('/',async  (req, res) => {
    //let{username} = req.body;
    //console.log(req.query.task_table_id)
    //console.log(req.params("table_task_id"));
    await getListtask(req.query.table_task_id).then((row) => {
        //console.log(row)
        res.send(row);
    });
    //res.send("/api/listtask")

});

module.exports = app;