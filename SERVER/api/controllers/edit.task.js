var knex = require('../knexdb');
var express = require('express');
var app = express();
var cors = require('cors');
var bodyparser = require('body-parser');
var edittask = require("../../services/edit.task");

app.use(cors());
app.use(bodyparser.json());

// function edittask(req, res) {
//     let { Id, title, description } = req.body;
//     // console.log(description);
//     knex('task')
//         .where('Id', '=', Id)
//         .update({
//             title: title,
//             description: description,
//         }).catch(
//             function (err) {
//                 console.log(err)
//             });
//     res.send({
//         authsuccess: true,
//         Status: true
//     });
// }


app.post('/',async  (req, res) => {
    console.log('submit your edition!'); let { Id, title, description } = req.body;
    await edittask(Id, title, description).catch(
        function (err) {
            console.log(err);
            res.send({
                authsuccess: true,
                Status: false
            })
        });
    res.send({
        authsuccess: true,
        Status: true
    });

})


module.exports = app;