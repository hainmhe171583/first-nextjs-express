var knex = require('../knexdb');
var express = require('express');
var app = express();
var cors = require('cors');
var bodyparser = require('body-parser');
var newtable = require("../../services/new.table");

app.use(cors());
app.use(bodyparser.json());

// function newtask(req, res) {
//     let { user_id, title } = req.body;
//     knex.insert([
//         {
//             user_id: user_id,
//             title: title
//         }
//     ]).into('table_task')
//     .catch(function (err) {
//         console.log(err);
//     });
//     res.send({
//         authsuccess: true,
//         Status: true
//     });
// }

app.post('/', async (req, res) => {
    console.log('submited for new table task!!');
    let { user_id, title } = req.body;
    await newtable(user_id, title)
    .catch(function (err) {
        console.log(err);
    });
    res.send({
        authsuccess: true,
        Status: true
    });
});

module.exports = app;