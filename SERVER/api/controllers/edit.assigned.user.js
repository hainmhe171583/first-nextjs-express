var knex = require('../knexdb');
var express = require('express');
var app = express();
var cors = require('cors');
var bodyparser = require('body-parser');
var jwt = require("jsonwebtoken");
var userInfor = require("./get.userinfo")
var editAssignUser = require("../../services/edit.assigned.user");


app.use(cors());
app.use(bodyparser.json());

// async function editAssignUser(req, res) {
//     let { task_id, assignUser } = req.body;
//     // console.log(description);
//     let assignId = await userInfor(null, assignUser).then((row) => { if (row.length != 0) return row[0].Id });
//     console.log(assignId);
//     if (assignId == undefined)
//         return res.send({
//             authsuccess: true,
//             Status: false,
//             message: 'not found user'
//         })
//     else
//         knex('task')
//             .where('Id', '=', task_id)
//             .update({
//                 assigned_user_id: assignId
//             }).catch(
//                 function (err) {
//                     console.log(err)

//                 });
//     return res.send({
//         authsuccess: true,
//         Status: true
//     });
// }


app.post('/', async (req, res) => {
    console.log('submit your edition!');
    let { task_id, assignUser } = req.body;
    await editAssignUser(task_id, assignUser)
        .catch(
            function (err) {
                console.log(err)

            });
    return res.send({
        authsuccess: true,
        Status: true
    });

    // console.log(data);
    // res.send({
    //     data,
    //     authsuccess: true,
    //     Status: true
    // });
})


module.exports = app;