var express = require('express');
var routes = express.Router();
var ListTaskController = require('./list.task');
var EditTaskController = require('./edit.task');
var NewTaskController = require('./new.task');
var DeleteTaskController = require('./delete.task');
var ListTableController = require('./list.table');
var AssignedTableController = require('./assigned.table')
var AssignedUserController = require('./assigned.user');
var DeleteAssignedUserController = require('./delete.assigned.user');
var EditStatusController = require('./edit.status');
var EditAssignedUserController = require('./edit.assigned.user');
var NewTableController = require('./new.table');



routes.use('/listtask', ListTaskController);
routes.use('/edittask', EditTaskController);
routes.use('/newtask', NewTaskController);
routes.use('/deletetask', DeleteTaskController);
routes.use('/listtable', ListTableController);
routes.use('/assignedtable',AssignedTableController);
routes.use('/assigneduser',AssignedUserController);
routes.use('/deleteassigneduser', DeleteAssignedUserController);
routes.use('/editstatus',EditStatusController);
routes.use('/editassigneduser',EditAssignedUserController);
routes.use('/newtable',NewTableController);



module.exports = routes;