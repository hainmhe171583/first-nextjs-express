var knex = require('../knexdb');
var express = require('express');
var app = express();
var cors = require('cors');
var bodyparser = require('body-parser');
var jwt = require("jsonwebtoken");
var getListTable = require("../../services/assigned.table");

app.use(cors());
app.use(bodyparser.json());

// function getUser(req, res) {
//     var tokenString = JSON.stringify(req.headers['tokenstring']);
//     tokenString = tokenString.substring(1, tokenString.length-1);

//     try {
//         const Id = jwt.verify(tokenString, process.env.ACCESS_TOKEN_SECRET).Id;
//         return Id
//     } catch (err) {
//         console.log(err);
//         return -1;
//     }
// }

// function getListTable(req, res) {
//     // var username = getUser(req, res);
//     knex('task')
//         .where({ assigned_user_id: req.query.user_id })
//         .join('table_task','task.table_task_Id' , '=', 'table_task.Id')
//         .join('user', 'table_task.user_id','=','user.Id')
//         .select('task.Id','task.table_task_id','task.title','task.description', 'task.status_id', 'user.username','table_task.title as task_table')
//         .then((row) => {
//             res.send(row);
//         })
// }





app.get('/', async (req, res) => {
    //let{username} = req.body;
    await getListTable(req.query.user_id)
        .then((row) => {
            res.send(row);
        });
    //res.send("/api/listtask")

});

module.exports = app;