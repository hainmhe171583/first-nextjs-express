var knex = require('../knexdb');
var express = require('express');
var app = express();
var cors = require('cors');
var bodyparser = require('body-parser');
var jwt = require("jsonwebtoken");
var getListTable = require("../../services/assigned.user");

app.use(cors());
app.use(bodyparser.json());

// function getUser(req, res) {
//     var tokenString = JSON.stringify(req.headers['tokenstring']);
//     tokenString = tokenString.substring(1, tokenString.length-1);

//     try {
//         const Id = jwt.verify(tokenString, process.env.ACCESS_TOKEN_SECRET).Id;
//         return Id
//     } catch (err) {
//         console.log(err);
//         return -1;
//     }
// }

// function getListTable(req, res) {
//     // var username = getUser(req, res);
//     // console.log(req.query.table_task_id);
//     knex('table_task')
//         .where({ 'table_task.Id': req.query.table_task_id })
//         .join('assign', 'assign.table_task_id', '=', 'table_task.Id')
//         .join('user','assign.user_id','=','user.Id')
//         .select('assign.user_id','user.username')
//         .then((row) => {
//             res.send(row);
//         })
// }





app.get('/', async (req, res) => {
    //let{username} = req.body;
    await getListTable(req.query.table_task_id)
        .then((row) => {
            res.send(row);
        });
    //res.send("/api/listtask")

});

module.exports = app;