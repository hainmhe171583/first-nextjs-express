var knex = require('../knexdb');
var express = require('express');
var app = express();
var cors = require('cors');
var bodyparser = require('body-parser');
var jwt = require("jsonwebtoken");
var getUser = require("../../ultis/get.userby.token");
var getListTable = require("../../services/list.table");


app.use(cors());
app.use(bodyparser.json());

// function getUser(req, res) {
//     var tokenString = JSON.stringify(req.headers['tokenstring']);
//     tokenString = tokenString.substring(1, tokenString.length-1);

//     try {
//         const username = jwt.verify(tokenString, process.env.ACCESS_TOKEN_SECRET).username;
//         return username
//     } catch (err) {
//         console.log(err);
//         return -1;
//     }
// }

// function getListTable(req, res) {
//     var username = getUser(req, res);
//     knex('table_task')
//         .where({ username: username })
//         .join('user', 'user.Id', '=', 'table_task.user_id')
//         .select('table_task.Id','table_task.title')
//         .then((row) => {
//             res.send(row);
//         })
// }





app.get('/', async (req, res) => {
    //let{username} = req.body;
    var username = getUser(req, res);
    await await getListTable(username)
        .then((row) => {
            res.send(row);
        });
    //res.send("/api/listtask")

});

module.exports = app;