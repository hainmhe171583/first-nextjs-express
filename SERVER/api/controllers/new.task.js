var knex = require('../knexdb');
var express = require('express');
var app = express();
var cors = require('cors');
var bodyparser = require('body-parser');
var jwt = require("jsonwebtoken");
var newtask = require("../../services/new.table");

app.use(cors());
app.use(bodyparser.json());

// function newtask(req, res) {
//     let { table_task_id, title, description } = req.body;
//     knex.insert([
//         {
//             table_task_id: table_task_id,
//             title: title,
//             description: description,
//             status_id: 1
//         }
//     ]).into('task').catch(function (err) {
//         console.log(err);
//     });
//     res.send({
//         authsuccess: true,
//         Status: true
//     });
// }

app.post('/', async (req, res) => {
    console.log('submited for new task!!');
    let { table_task_id, title, description } = req.body;
    await newtask(table_task_id, title, description).catch(function (err) {
        console.log(err);
        res.send({
            authsuccess: true,
            Status: false
        })
    });
    res.send({
        authsuccess: true,
        Status: true
    });
});

module.exports = app;