var knex = require('../knexdb');
var express = require('express');
var app = express();
var cors = require('cors');
var bodyparser = require('body-parser');
var jwt = require("jsonwebtoken");
var deletedAssign = require("../../services/delete.assigned.user");

app.use(cors());
app.use(bodyparser.json());

// function deletetask(req, res) {
//     let { table_task_id, user_id } = req.body;
//     //console.log(Id);
    
//     knex('assign')
//         .where('table_task_id',table_task_id)
//         .andWhere('user_id',user_id)
//         .del()
//         .catch(
//         function (err) {
//             console.log(err)
//         });
        
//     res.send({
//         authsuccess: true,
//         Status: true
//     });
// }

app.post('/', async (req, res) => {
    console.log('submited for delete a task!!');
    let { table_task_id, user_id } = req.body;
    await deletedAssign(table_task_id, user_id)
        .catch(
        function (err) {
            console.log(err);
            return res.send({
                authsuccess: true,
                Status: false
            });
        });
        
    return res.send({
        authsuccess: true,
        Status: true
    });
// }
});

module.exports = app;