require('dotenv').config()
const jwt = require('jsonwebtoken')
const express = require('express');
var bodyparser = require('body-parser');
const app = express();
//const port = 3000;

app.use(express.json());
app.use(bodyparser.json());

// app.get('/', (req, res) => {
//     const data = req.body;
//     const accessToken = jwt.sign(data, process.env.ACCESS_TOKEN_SECRET, {
//       expiresIn: '1d',
//     });
//     //let { username, password } = req.body;
//     console.log(req.body);
//     res.json({ accessToken });
// })


var token = (data) => {
    // const data = req.body;
    const accessToken = jwt.sign(data, process.env.ACCESS_TOKEN_SECRET, {
      expiresIn: '1d',
    });
    // let { username, password } = req.body;
    //console.log(username);
    //res.json({ accessToken });
    return accessToken;
}

module.exports = token;