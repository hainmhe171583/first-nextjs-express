var express = require('express');
var app = express();
var bodyparser = require('body-parser');
var jwt = require("jsonwebtoken");

app.use(bodyparser.json());

function getUser(req, res) {
    var tokenString = JSON.stringify(req.headers['tokenstring']);
    tokenString = tokenString.substring(1, tokenString.length-1);

    try {
        const username = jwt.verify(tokenString, process.env.ACCESS_TOKEN_SECRET).username;
        return username
    } catch (err) {
        console.log(err);
        return -1;
    }
}

module.exports = getUser;