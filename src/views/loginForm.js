import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import React, { useEffect, useState } from 'react';




function LoginForm() {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  function sendData(event) {
    event.preventDefault();
    var userInfo = {
      username: username,
      password: password
    }
    fetch("http://127.0.0.1:8080/api/auth/login", {
      method: 'post',
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(userInfo)
    }).then(
      response => response.json()
    ).then(
      data => {
        console.log(JSON.stringify(data));
        if (data.authsuccess == true) {
          localStorage.setItem('tokenString', data.tokenString);
          localStorage.setItem('expires', data.expires);
          localStorage.setItem('username', data.username);
          localStorage.setItem('userId',data.userId);
          //console.log(localStorage.getItem('tokenInfo'));
          window.location.href = '/';
        }
      })

    console.log("click");
  }


  console.log("login form loading!!");

  return (


    <Form onSubmit={sendData} style={{
      width: "30%",
      background: "#d9d2c28a",
      padding: "40px",
      margin: "0px auto",
    }} >
      
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>User name</Form.Label>
        <Form.Control type="text" placeholder="Enter Username" name="username" onChange={e => setUsername(e.target.value)} />

      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicPassword">
        <Form.Label>Password</Form.Label>
        <Form.Control type="password" placeholder="Password" name="password" onChange={e => setPassword(e.target.value)} />
      </Form.Group>
      <Button variant="primary" type="submit">
        Submit
      </Button>

    </Form>
  );
}

export default LoginForm;
