import React, { useEffect, useState } from 'react';
import Table from 'react-bootstrap/Table';
import Modal from 'react-bootstrap/Modal';
import { Outlet, Link } from "react-router-dom";
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import '../assets/listwork.css';
import GenericModal from '../modal/popup.detail.work';
import ConfirmModal from '../modal/popup.confirm';


const UpdateTodoForm = (data, sendData, onFormChange) => {
    const [title, setTitle] = useState(data.title);
    const [description, setDescription] = useState(data.description);
    // console.log(sendData, onFormChange)
    return (<Form onSubmit={(e) => { sendData(data.Id, e) }}>

        <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
            <Form.Label>Title</Form.Label>
            <Form.Control
                type="text"
                placeholder="Your title"
                autoFocus
                value={title}
                name={'title'}
                onChange={(e) => {
                    setTitle(e.target.value);
                    data.title = e.target.value;
                    console.log(e.target.value);
                    //onFormChange('title', e);
                }}
            />
        </Form.Group>
        <Form.Group
            className="mb-3"
            controlId="exampleForm.ControlTextarea1">
            <Form.Label>Detail</Form.Label>
            <Form.Control
                as="textarea"
                rows={3}
                value={description}
                name={'description'}

                onChange={(e) => {
                    setDescription(e.target.value);
                    data.description = e.target.value;
                    console.log(e.target.value);

                    //onFormChange('description', e)
                }}

            />
        </Form.Group>

    </Form>)
}


const NewWorkForm = (data, sendNewData) => {
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    // console.log(sendData, onFormChange)



    return (<Form onSubmit={(e) => { sendNewData(e) }}>

        <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
            <Form.Label>Title</Form.Label>
            <Form.Control
                type="text"
                placeholder="Your title"
                autoFocus
                value={title}
                name={'title'}
                onChange={(e) => {
                    setTitle(e.target.value);
                    console.log(e.target.value);
                    data.title = e.target.value;
                    //onFormChange('title', e);
                }}
            />
        </Form.Group>
        <Form.Group
            className="mb-3"
            controlId="exampleForm.ControlTextarea1">
            <Form.Label>Detail</Form.Label>
            <Form.Control
                as="textarea"
                rows={3}
                value={description}
                name={'description'}

                onChange={(e) => {
                    setDescription(e.target.value);
                    console.log(e.target.value);
                    data.description = e.target.value;
                    //onFormChange('description', e)
                }}

            />
        </Form.Group>

    </Form>)
}







function Listwork() {
    const [loginCheck, setLoginCheck] = useState(true);
    const [modalShow, setModalShow] = useState(false);
    const [confirmshow, setConfirmshow] = useState(false);
    const [addworkmodal, setAddworkmodal] = useState(false);
    const [currentFormState, setCurrentFormState] = useState({
        Id: '',
        title: '',
        description: ''
    })
    const [deleteid, setDeleteid] = useState();
    useEffect(
        () => {
            if (localStorage.getItem('tokenString'))
                setLoginCheck(true);
            else
                setLoginCheck(false);
        },
        [loginCheck]);
    const [dataset, setDataset] = useState([]);
    useEffect(() => {
        //const token = localStorage.getItem('tokenString')
        //console.log('tokennnnnn', token)
        fetch("http://127.0.0.1:8080/api/work/listwork", {

            headers: {
                "Content-Type": "application/json",
                "tokenString": localStorage.getItem('tokenString'),
                "expires": localStorage.getItem('expires')
            }
            //body: localStorage.getItem('tokenInfo')
        })
            .then(function (res) {
                return res.json();

            })
            .then(function (data) {
                console.log(data);
                if (data.success == false) {
                    if (data.code == 'UNAUTH') {
                        localStorage.removeItem('tokenString');
                        //console.log(localStorage.getItem('tokenInfo'));
                        window.location.href = '/login';
                    }
                }
                else
                    setDataset(data);
            })
            .catch(
                e => console.log(e)
            );
    }, []);

    function deleteWork(Id) {
        console.log(Id);
        fetch("http://127.0.0.1:8080/api/work/deletework", {
            method: 'post',
            headers: {
                "Content-Type": "application/json",
                "tokenString": localStorage.getItem('tokenString'),
                "expires": localStorage.getItem('expires')
            },
            body: JSON.stringify({ Id: Id })
        })
            .then(function (res) {
                return res.json();

            })
            .then(function (data) {
                console.log(data);
                if (data.success == false) {
                    if (data.code == 'UNAUTH') {
                        localStorage.removeItem('tokenString');
                        //console.log(localStorage.getItem('tokenInfo'));
                        window.location.href = '/login';
                    }
                }
                window.location.href = '/mylist';
            })
            .catch(
                e => console.log(e)
            );
    }
    function sendData(itemId, e) {

        //console.log(itemId);
        var workDetail = {
            Id: itemId,
            title: currentFormState.title,
            description: currentFormState.description
        }
        fetch("http://127.0.0.1:8080/api/work/editwork", {
            method: 'post',
            headers: {
                "Content-Type": "application/json",
                "tokenString": localStorage.getItem('tokenString'),
                "expires": localStorage.getItem('expires')
            },
            body: JSON.stringify(workDetail)
        }).then(
            response => response.json()
        ).then(
            data => {

                console.log(JSON.stringify(data));
                setModalShow(false);
                if (data.authsuccess == false) {
                    localStorage.removeItem('tokenString');
                    //console.log(localStorage.getItem('tokenInfo'));
                    window.location.href = '/login';
                }
                window.location.reload();
            })

        console.log("click");
    }

    function sendNewData(data, event) {
        // event.preventDefault();
        console.log('dsadsadsa');
        var workDetail = {
            userId: localStorage.userId,
            title: currentFormState.title,
            description: currentFormState.description
        }
        fetch("http://127.0.0.1:8080/api/work/newwork", {
            method: 'post',
            headers: {
                "Content-Type": "application/json",
                "tokenString": localStorage.getItem('tokenString'),
                "expires": localStorage.getItem('expires')
            },
            body: JSON.stringify(workDetail)
        }).then(
            response => response.json()
        ).then(
            data => {

                console.log(JSON.stringify(data));
                if (data.authsuccess == false) {
                    localStorage.removeItem('tokenString');
                    //console.log(localStorage.getItem('tokenInfo'));
                    window.location.href = '/login';
                }
                window.location.href = '/mylist';
            })

        console.log("click");
    }



    const onFormChange = (key, e) => {
        //console.log(key);
        e.preventDefault();
        setCurrentFormState({
            ...currentFormState,
            [key]: e.target.value
        })

    }
    const activeModal = (data) => {
        setModalShow(true);
        setCurrentFormState(data);
    }

    const activeModalConfirm = (data) => {
        setConfirmshow(true);
        setDeleteid(data);
    }

    const activeModalNew = () => {
        setAddworkmodal(true);
    }
    if (loginCheck == true)
        return (
            <>
            
                <Table striped bordered hover variant="dark" style={{ width: "80%", margin: "0px auto" }}>
                     
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th style={{ width: "10%" }}>Option</th>

                        </tr>
                    </thead>
                    <tbody>
                        {
                            dataset.map((data, index) => {

                                return <tr key={data.Id}>
                                    <td>{index + 1}</td>
                                    <td>{data.title}</td>
                                    <td style={{ display: "flex" }}>
                                        {/* <Detail var data={data} />&nbsp; */}
                                        {/* <Detail data={data} status="true" /> */}
                                        <Button variant="primary" onClick={() => activeModal(data)}> DETAIL</Button>&nbsp;
                                        {/* <Button variant="primary"><Link to="/" className="nav-link"> EDIT </Link></Button>&nbsp; */}
                                        <Button variant="primary" onClick={() => activeModalConfirm(data.Id)}> DELETE </Button>
                                    </td>
                                </tr>
                            })
                        }
                    </tbody>
                    <Button variant="primary" onClick={() => setAddworkmodal(true)}> ADD a new work </Button>  
                </Table>
                {/* <GenericModal data={currentFormState} show={modalShow} onHide={() => setModalShow(false)} title={'YOUR work detail'} Body={() => UpdateTodoForm(currentFormState, sendData, onFormChange)} onSubmit={() => sendData(currentFormState.Id)} onClose={() => { setModalShow(false) }} /> */}
                {GenericModal({ data: currentFormState, show: modalShow, onHide: () => setModalShow(false), title: 'YOUR work detail', Body: () => UpdateTodoForm(currentFormState, sendData, onFormChange), onSubmit: () => sendData(currentFormState.Id), onClose: () => { setModalShow(false) } })}
                <ConfirmModal cfShow={confirmshow} title='Are you sure to delete?' Body='If you confirm you cant get back!' onClick={() => deleteWork(deleteid)} handleClose={() => setConfirmshow(false)} />

                
                {GenericModal({
                    show: addworkmodal, onHide: () => setAddworkmodal(false), title: 'Add your new work', Body: () => NewWorkForm(currentFormState, sendNewData), onSubmit: () => sendNewData(), onClose: () => { setAddworkmodal(false) }
                })}

            </>

        );
    else
        window.location.href = '/login';

}

export default Listwork;