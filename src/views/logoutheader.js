import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import { Outlet, Link } from "react-router-dom";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import LoginForm from './loginForm';
import RegisterForm from './registerForm';


function logout(){

}

function logOutHeader() {
  return (
    <>
    <div>
      <Navbar expand="lg" className="bg-body-tertiary">
        <Container>
          <Navbar.Brand href="#home">Todo website</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
              <Link to="/" className="nav-link"> Home </Link>
              <Link to="/login" className="nav-link"> Login </Link>
              <Link to="/register" className="nav-link"> Register </Link>

            </Nav>
          </Navbar.Collapse>
        </Container>

      </Navbar>
      </div>
      {/* <div>
        <Routes>
          <Route path="/login" element={<LoginForm />}>  </Route>
          <Route path="/register" element={<RegisterForm />}>  </Route>
        </Routes>
      </div> */}
    </>
  );
}

export default logOutHeader;