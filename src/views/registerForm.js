import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import React, { useEffect, useState } from 'react';

function RegisterForm() {
  const [alert,setAlert] = useState('');
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [repassword, setRepassword] = useState('');

  function sendData(event) {
    event.preventDefault();
    if (password === repassword) {
      var userInfo = {
        username: username,
        password: password
      }
      fetch("http://127.0.0.1:8080/api/auth/register", {
        method: 'post',
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(userInfo)
      }).then(
        response => response.json()
      ).then(
        data => {
          console.log(JSON.stringify(data));
          if (data.authsuccess == true) {
            localStorage.setItem('tokenString', data.tokenString);
            localStorage.setItem('expires', data.expires);
            //console.log(localStorage.getItem('tokenInfo'));
            window.location.href = '/';
          }
          else
          {
            
            setAlert(data.message);
          }
        })

    }


    console.log("click");
  }

  useEffect(() =>{
    if(repassword != password && repassword!='' && password!='')
      setAlert('Re-enter password must be matched');
    else
      setAlert('');
  },[repassword][password]);



  console.log("register form loading!!")

  return (
    <div className="register">
      <Form onSubmit={sendData} style={{
      width: "30%",
      background: "#d9d2c28a",
      padding: "40px",
      margin: "0px auto"
    }}>
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>User name</Form.Label>
          <Form.Control type="text" placeholder="Enter Username" onChange={e => setUsername(e.target.value)} />

        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control type="password" placeholder="Password" onChange={e => setPassword(e.target.value)} />
        </Form.Group>
        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Re-Enter Password</Form.Label>
          <Form.Control type="password" placeholder="Re-Enter Password" onChange={e => setRepassword(e.target.value)}/>
        </Form.Group>
        <Button variant="primary" type="submit">
          Submit
        </Button>
      <h4>{alert}</h4>
      </Form>
    </div>
  );
}

export default RegisterForm;
