import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import { Outlet, Link } from "react-router-dom";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import LoginForm from './loginForm';

function Loggedheader() {

  function logout(){
    localStorage.removeItem('tokenString');
    localStorage.removeItem('expires');
    window.location.replace('/');
  }
  return (


    <Navbar expand="lg" className="bg-body-tertiary">
      <Container>
        <Navbar.Brand href="/">Hello {localStorage.getItem('username')}</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Link to="/newwork" className="nav-link" >New work</Link>
            <Link to="/mylist" className="nav-link">My works</Link>
            <NavDropdown title="Dropdown" id="basic-nav-dropdown">
              <NavDropdown.Item href="#action/3.4">
                change password
              </NavDropdown.Item>           
              <NavDropdown.Divider />
              <NavDropdown.Item onClick={() => logout()} >Log out</NavDropdown.Item>
            </NavDropdown>
          </Nav>
        </Navbar.Collapse>
      </Container>

    </Navbar>



  );
}

export default Loggedheader;