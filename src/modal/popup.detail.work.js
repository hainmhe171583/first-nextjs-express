import { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';

function GenericModal({data, show, onHide, title, Body, onSubmit, onClose}) {

    // const [show, setShow] = useState(data.status);
    // const [title, setTitle] = useState(data.data.title);
    // const [description, setDescription] = useState(data.data.description);
    // const handleClose = () => setShow(false);
    // const handleShow = () => setShow(true);
    //console.log(data.data.title);

    // function sendData(event) {
    //     event.preventDefault();
    //     console.log(data.data.Id)
    //     var workDetail = {
    //         Id: data.data.Id,
    //         title: title,
    //         description: description
    //     }
    //     fetch("http://127.0.0.1:8080/api/work/editwork", {
    //         method: 'post',
    //         headers: {
    //             "Content-Type": "application/json",
    //             "tokenString" : localStorage.getItem('tokenString'),
    //             "expires" : localStorage.getItem('expires')
    //         },
    //         body: JSON.stringify(workDetail)
    //     }).then(
    //         response => response.json()
    //     ).then(
    //         data => {

    //             console.log(JSON.stringify(data));
    //             setShow(false);
    //             if (data.authsuccess == false) {
    //                 localStorage.removeItem('tokenString');
    //                 //console.log(localStorage.getItem('tokenInfo'));
    //                 window.location.href = '/login';
    //             }
    //             window.location.reload();
    //         })

    //     console.log("click");
    // }


    return (
        <>
            {/* <Button variant="primary" onClick={handleShow}>
                DETAIL
            </Button> */}

            <Modal show={show} onHide={onHide}>
                <Modal.Header closeButton>
                    <Modal.Title>{title}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                {/* <Form onSubmit={sendData}>
                        <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                            <Form.Label>Title</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Your title"
                                autoFocus
                                value={title}
                                onChange={e => setTitle(e.target.value)}
                            />
                        </Form.Group>
                        <Form.Group
                            className="mb-3"
                            controlId="exampleForm.ControlTextarea1">
                            <Form.Label>Detail</Form.Label>
                            <Form.Control
                                as="textarea"
                                rows={3}
                                value={description}
                                onChange={e => setDescription(e.target.value)}
                            />
                        </Form.Group>
                    </Form> */}
                    <Body></Body>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={onClose}>
                        Close
                    </Button>
                    <Button variant="primary" onClick={onSubmit}>
                        Save Changes
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}

export default GenericModal;