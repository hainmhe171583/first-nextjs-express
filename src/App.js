import logo from './logo.svg';
import './App.css';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { Outlet, Link } from "react-router-dom";
import LoginPage from './views/loginForm';
import Header from './views/header';
import RegisterForm from './views/registerForm';
import Home from './views/home';
import Listwork from './views/listwork';
import Newwork from './views/newwork';
import 'bootstrap/dist/css/bootstrap.min.css';


function App() {
  return (
    <BrowserRouter>
      <Header />
      <Routes>
        <Route path="/" element={<Home />}>   </Route>
        <Route path="/mylist" element={<Listwork />}>   </Route>
        <Route path="/newwork" element={<Newwork />}>   </Route>
        <Route path="/login" element={<LoginPage />} >   </Route>
        <Route path="/register" element={<RegisterForm />} >   </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
