'use client'
import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { useEffect, useState } from 'react';
import Button from '@mui/material/Button';
import DetailModal from '../components/DetailModal';
import { useSearchParams } from 'next/navigation';
import Link from 'next/link';
import ChipStatus from '../components/ChipStatus';

// function createData(name, calories, fat, carbs, protein) {
//     return { name, calories, fat, carbs, protein };
// }

// const rows = [
//     createData('Frozen yoghurt', 159, 6.0, 24, 4.0),
//     createData('Ice cream sandwich', 237, 9.0, 37, 4.3),
//     createData('Eclair', 262, 16.0, 24, 6.0),
//     createData('Cupcake', 305, 3.7, 67, 4.3),
//     createData('Gingerbread', 356, 16.0, 49, 3.9),
// ];

// async function getData() {
//     if (typeof window !== "undefined")
//         {
//             console.log(localStorage.getItem('tokenString'));
//             return localStorage.getItem('tokenString');
//         }
//console.log(typeof window);

// const res = await fetch('http://127.0.0.1:8080/api/task/listtask') 
// {
//     headers: {
//         "Content-Type": "application/json",
//         "tokenString": localStorage.getItem('tokenString'),
//         "expires": localStorage.getItem('expires')
//     }
//     //body: localStorage.getItem('tokenInfo')
// })


// if (!res.ok) {
// This will activate the closest `error.js` Error Boundary
//         throw new Error('Failed to fetch data')
//     }

//     return res.json()
// }

export default function BasicTable({data}) {
    const searchParams = useSearchParams()
    const [dataset, setDataset] = useState([]);
    const [openDetailModal, setOpenDetailModal] = useState(false);
    const [rowdata, setRowdata] = useState({});

    useEffect(() => {
        // console.log( searchParams.get('table_task_id'))
        fetch("http://127.0.0.1:8080/api/task/assignedtable?" + new URLSearchParams({
            user_id: localStorage.getItem('userId').toString(),
        }).toString(), {

            headers: {
                "Content-Type": "application/json",
                "tokenString": localStorage.getItem('tokenString'),
                "expires": localStorage.getItem('expires')
            }
        })
            .then(function (res) {
                return res.json();

            })
            .then(function (data) {
                console.log(data);
                if (data.success == false) {
                    if (data.code == 'UNAUTH') {
                        localStorage.removeItem('tokenString');
                        window.location.href = '/login';
                    }
                }
                else
                    setDataset(data);
            })
            .catch(
                e => console.log(e)
            );
    }, []);

    function openModal(row) {
        // alert(JSON.stringify(row));
        setOpenDetailModal(true);
        setRowdata(row)
        // return (<DetailModal />);
    }

    return (


        <>
            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell align="left">STT</TableCell>
                            <TableCell>Title </TableCell>
                            <TableCell align="center"> By </TableCell>
                            <TableCell align="center"> Contain in </TableCell>
                            <TableCell align="center"> status </TableCell>
                            <TableCell align="right">Action</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {dataset.map((row, index) => (
                            <TableRow
                                key={row.Id}
                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                            >
                                <TableCell component="th" scope="row">
                                    {index + 1}
                                </TableCell>
                                <TableCell component="th" scope="row">
                                    {row.title}
                                </TableCell>
                                <TableCell align='center' component="th" scope="row">
                                    {row.username}
                                </TableCell>
                                <TableCell align='center' component="th" scope="row">
                                <Link href={{
                                        pathname: 'mylist',
                                        query: { table_task_id: row.table_task_id }
                                    }}>
                                        {row.task_table}
                                    </Link>
                                </TableCell>
                                <TableCell component="th" scope="row">
                                    <ChipStatus status_id={row.status_id} />
                                </TableCell>
                                <TableCell align="right">
                                    {
                                        true &&   <Button variant="contained" onClick={()=>openModal(row)}>Detail</Button>
                                    }
                                
                              
                                    {/* <DetailModal /> */}
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
            <DetailModal data={rowdata} open={openDetailModal} handleClose={() => setOpenDetailModal(false)} />
        </>
    );
}
