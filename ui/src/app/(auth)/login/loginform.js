'use client'
import * as React from 'react';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import { useState, useEffect } from 'react';


export default function LoginForm() {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');


    function sendLoginRequest(event) {
        event.preventDefault();
        console.log(username);
        var userInfo = {
          username: username,
          password: password
        }
        fetch("http://127.0.0.1:8080/api/auth/login", {
          method: 'post',
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify(userInfo)
        }).then(
          response => response.json()
        ).then(
          data => {
            console.log(JSON.stringify(data));
            if (data.authsuccess == true) {
              localStorage.setItem('tokenString', data.tokenString);
              localStorage.setItem('expires', data.expires);
              localStorage.setItem('username', data.username);
              localStorage.setItem('userId',data.userId);
              //console.log(localStorage.getItem('tokenInfo'));
              window.location.href = '/';
            }
          })
    
        console.log("click");
      }
    
    
      console.log("login form loading!!");

    return (

        <>
            <Typography
                variant="h6"
                noWrap
                component="a"
                sx={{
                    mr: 2,
                    display: { xs: 'none', md: 'flex' },
                    fontFamily: 'monospace',
                    fontWeight: 200,
                    letterSpacing: '.1rem',
                    color: 'inherit',
                    textDecoration: 'none',
                }}
            >
                Username
            </Typography>
            <TextField
                required
                id="outlined-required"
                label="Required"
                placeholder="username"
                onChange={e => setUsername(e.target.value)}
                sx={{ width: '100%' }} />

            <Typography
                variant="h6"
                noWrap
                component="a"
                sx={{
                    mr: 2,
                    display: { xs: 'none', md: 'flex' },
                    fontFamily: 'monospace',
                    fontWeight: 200,
                    letterSpacing: '.1rem',
                    color: 'inherit',
                    textDecoration: 'none',
                }}

            >
                Password
            </Typography>
            <TextField
                required
                id="outlined-password-input"
                label="Password"
                type="password"
                placeholder="password"
                autoComplete="current-password"
                onChange={e => setPassword(e.target.value)}
                sx={{ width: '100%' }} 
            />
            <Button variant="contained" onClick={sendLoginRequest}>login</Button>
        </>
    )
}