import * as React from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import LoginForm from "./loginform";
export default function Login() {
    return (
        <>
            <React.Fragment>
                <CssBaseline />
                <Container maxWidth="sm">

                    <Box sx={{ bgcolor: '#d3d3cb', height: '300px', padding: '40px' }} >
                        <LoginForm/>
                    </Box>

                </Container>
            </React.Fragment>
        </>
    );
}
