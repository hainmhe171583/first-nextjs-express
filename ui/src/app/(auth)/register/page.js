import * as React from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import RegisterForm from "./registerform";

export default function Register() {
    return (
        <>
            <>
                <React.Fragment>
                    <CssBaseline />
                    <Container maxWidth="sm">

                        <Box sx={{ bgcolor: '#d3d3cb', height: '400px', padding: '40px' }} >
                            <RegisterForm/>
                        </Box>

                    </Container>
                </React.Fragment>
            </>
        </>
    );
}
