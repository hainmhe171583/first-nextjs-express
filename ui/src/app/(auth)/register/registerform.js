'use client'
import * as React from 'react';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import { useState, useEffect } from 'react';


export default function RegisterForm() {
    const [alert, setAlert] = useState('');
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [repassword, setRepassword] = useState('');

    function sendRegisterRequest(event) {
        event.preventDefault();
        if (password === repassword) {
            var userInfo = {
                username: username,
                password: password
            }
            fetch("http://127.0.0.1:8080/api/auth/register", {
                method: 'post',
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(userInfo)
            }).then(
                response => response.json()
            ).then(
                data => {
                    console.log(JSON.stringify(data));
                    if (data.authsuccess == true) {
                        localStorage.setItem('tokenString', data.tokenString);
                        localStorage.setItem('expires', data.expires);
                        //console.log(localStorage.getItem('tokenInfo'));
                        window.location.href = '/';
                    }
                    else {

                        setAlert(data.message);
                    }
                })

        }
    }
    return (

        <>
            <Typography
                variant="h6"
                noWrap
                component="a"
                sx={{
                    mr: 2,
                    display: { xs: 'none', md: 'flex' },
                    fontFamily: 'monospace',
                    fontWeight: 200,
                    letterSpacing: '.1rem',
                    color: 'inherit',
                    textDecoration: 'none',
                }}
                
            >
                Username
            </Typography>
            <TextField
                required
                id="outlined-required"
                label="Required"
                placeholder="username"
                sx={{ width: '100%' }} 
                onChange={e => setUsername(e.target.value)}
                />

            <Typography
                variant="h6"
                noWrap
                component="a"
                sx={{
                    mr: 2,
                    display: { xs: 'none', md: 'flex' },
                    fontFamily: 'monospace',
                    fontWeight: 200,
                    letterSpacing: '.1rem',
                    color: 'inherit',
                    textDecoration: 'none',
                }}
            >
                Password
            </Typography>
            <TextField
                required
                id="outlined-password-input"
                label="Password"
                type="password"
                placeholder="password"
                autoComplete="current-password"
                sx={{ width: '100%' }} 
                onChange={e => setPassword(e.target.value)}
                />
            <Typography
                variant="h6"
                noWrap
                component="a"
                sx={{
                    mr: 2,
                    display: { xs: 'none', md: 'flex' },
                    fontFamily: 'monospace',
                    fontWeight: 200,
                    letterSpacing: '.1rem',
                    color: 'inherit',
                    textDecoration: 'none',
                }}
            >
                Confirm password
            </Typography>
            <TextField
                required
                id="outlined-password-input"
                label="Confirm Password"
                type="password"
                placeholder="Confirm password"
                autoComplete="current-password"
                sx={{ width: '100%' }} 
                onChange={e => setRepassword(e.target.value)}
                />
            <Button variant="contained" onClick={sendRegisterRequest}>Register</Button>
        </>
    )

}