import * as React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import TextField from '@mui/material/TextField';
import { useState, useEffect } from 'react';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';


const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

export default function BasicModal({ data, open, handleClose }) {
    const [assignUser, setAssignUser] = useState();
    // const [open, setOpen] = React.useState(false);
    // const handleOpen = () => setOpen(true);
    // const handleClose = () => setOpen(false);
    useEffect(() => {
        if(data.username)
            setAssignUser(data.username);
    }, [data]);
    

    function editAssign(itemId, e) {

        //console.log(itemId);
        var taskDetail = {
            task_id: data.Id,
            assignUser: assignUser
        }
        fetch("http://127.0.0.1:8080/api/task/editassigneduser", {
            method: 'post',
            headers: {
                "Content-Type": "application/json",
                "tokenString": localStorage.getItem('tokenString'),
                "expires": localStorage.getItem('expires')
            },
            body: JSON.stringify(taskDetail)
        }).then(
            response => response.json()
        ).then(
            data => {

                console.log(JSON.stringify(data));
                //setModalShow(false);
                if (data.authsuccess == false) {
                    localStorage.removeItem('tokenString');
                    //console.log(localStorage.getItem('tokenInfo'));
                    window.location.href = '/login';
                }
                window.location.reload();
            })

        console.log("click");
    }
    return (
        <div>
            {/* <Button variant="contained" onClick={handleOpen}>Open modal</Button> */}
            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography id="modal-modal-title" variant="h6" component="h2">
                        Assign for
                    </Typography>
                    <TextField
                        required
                        id="outlined-required"
                        label="Required"
                        placeholder="username"
                        onChange={e => localStorage.getItem('userId') == data.user_id && setAssignUser(e.target.value)}
                        value={assignUser}
                        sx={{ width: '100%' }} />
                    {
                        localStorage.getItem('userId') == data.user_id && <Button variant="contained" onClick={editAssign}>Save change</Button>
                    }
                    

                </Box>
            </Modal>
        </div>
    );
}
