import * as React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import TextField from '@mui/material/TextField';
import { useState, useEffect } from 'react';
import { useSearchParams } from 'next/navigation'
const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

export default function BasicModal() {
    const [open, setOpen] = React.useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);
    const [title, setTitle] = useState();
    const [description, setDescription] = useState();
    const [status, setstatus] = useState();
    const searchParams = useSearchParams()

    // const [open, setOpen] = React.useState(false);
    // const handleOpen = () => setOpen(true);
    // const handleClose = () => setOpen(false);
    // useEffect(() => {
    //     setTitle(data.title);
    //     setDescription(data.description);
    //     setstatus(data.status_id);
    //     if (data.username)
    //         setAssignUser(data.username);
    // }, [data]);

    function NewTable(event) {
        event.preventDefault();
        var user_id = localStorage.getItem('userId');
        var tableDetail = {
            user_id: user_id,
            title: title
        }
        fetch("http://127.0.0.1:8080/api/task/newtable", {
            method: 'post',
            headers: {
                "Content-Type": "application/json",
                "tokenString": localStorage.getItem('tokenString'),
                "expires": localStorage.getItem('expires')
            },
            body: JSON.stringify(tableDetail)
        }).then(
            response => response.json()
        ).then(
            data => {

                console.log(JSON.stringify(data));
                if (data.authsuccess == false) {
                    localStorage.removeItem('tokenString');
                    //console.log(localStorage.getItem('tokenInfo'));
                    window.location.href = '/login';
                }
                window.location.reload();
            })

        console.log("click");
    }

    const handleChange = (event) => {
        setstatus(event.target.value);
        console.log(status);
    };

    return (
        <div>
            <Button variant="contained" color="secondary" onClick={handleOpen}>New Table</Button>
            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography id="modal-modal-title" variant="h6" component="h2">
                        Table Title
                    </Typography>
                    <TextField
                        required
                        id="outlined-required"
                        label="Required"
                        placeholder="title"
                        onChange={e => setTitle(e.target.value)}
                        value={title}
                        sx={{ width: '100%' }} />

                    {/* {
                        localStorage.getItem('userId') == data.user_id &&  */}
                    <>

                        <Button variant="contained" onClick={NewTable}>Save change</Button>
                    </>
                    {/* } */}
                    {/* {
                        localStorage.getItem('userId') != data.user_id && <>
                            <InputLabel id="demo-simple-select-label">Age</InputLabel>
                            <Select
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                value={status}
                                label="Age"
                                onChange={handleChange}
                            >
                                <MenuItem value={1}>On going</MenuItem>
                                <MenuItem value={2}>Done</MenuItem>
                                <MenuItem value={3}>Fail</MenuItem>
                            </Select>
                            <Button variant="contained" onClick={editStatus} >Save status</Button>
                        </>
                    } */}

                </Box>
            </Modal>
        </div>
    );
}
