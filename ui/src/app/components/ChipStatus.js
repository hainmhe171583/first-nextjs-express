import * as React from 'react';
import Chip from '@mui/material/Chip';
import Stack from '@mui/material/Stack';

export default function ColorChips({ status_id }) {
    if (status_id == 1)
        return (
            <Stack spacing={1} alignItems="center">
                <Stack direction="row" spacing={1}>
                    <Chip label="on going" color="primary" />
                </Stack>
            </Stack>
        )
    if(status_id==2)
        return (
            <Stack spacing={1} alignItems="center">
                <Stack direction="row" spacing={1}>
                    <Chip label="done" color="success" />
                </Stack>
            </Stack>
        )
        if(status_id==3)
            return (
                <Stack spacing={1} alignItems="center">
                    <Stack direction="row" spacing={1}>
                        <Chip label="fail" color="error" />
                    </Stack>
                </Stack>
            )
}