
var jwt = require('jsonwebtoken');


const checkLogin = () => {
    return (req, res, next) => {

        var tokenString = JSON.stringify(req.headers['tokenstring']);
        //console.log(tokenString)
        if (tokenString)
            tokenString = tokenString.substring(1, tokenString.length - 1);

        var expires = JSON.stringify(req.headers['expires']);
        if (expires)
            expires = expires.substring(1, expires.length - 1);
        //console.log('dsadsadsa');
        //console.log(jwt.verify(tokenString, process.env.ACCESS_TOKEN_SECRET));
        //console.log(tokenString);

        try {
            const user = jwt.verify(tokenString, process.env.ACCESS_TOKEN_SECRET);
            req.headers['user'] = user

            next();
            //res.send( true );
        } catch (err) {
            //console.log(err);
            res.send({
                success: false,
                message: "invalid or expired token",
                code: "UNAUTH"
            });
        }
    }
}

module.exports = { checkLogin }