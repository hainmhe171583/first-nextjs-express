var knex = require('../knexdb');
var express = require('express');
var app = express();
var cors = require('cors');
var bodyparser = require('body-parser');
var jwt = require("jsonwebtoken");


app.use(cors());
app.use(bodyparser.json());

function editWork(req, res) {
    let { Id, title, description } = req.body;
    // console.log(description);
    knex('work')
        .where('Id', '=', Id)
        .update({
            title: title,
            description: description,
        }).catch(
            function (err) {
                console.log(err)
            });
    res.send({
        authsuccess: true,
        Status: true
    });
}


app.post('/', (req, res) => {
    console.log('submit your edition!');
    editWork(req, res);

})


module.exports = app;