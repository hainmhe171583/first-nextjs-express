var knex = require('../knexdb');
var express = require('express');
var app = express();
var cors = require('cors');
var bodyparser = require('body-parser');
var jwt = require("jsonwebtoken");

app.use(cors());
app.use(bodyparser.json());


function getUser(req, res) {
    var tokenString = JSON.stringify(req.headers['tokenstring']);
    tokenString = tokenString.substring(1, tokenString.length-1);

    try {
        const username = jwt.verify(tokenString, process.env.ACCESS_TOKEN_SECRET).username;
        return username
    } catch (err) {
        console.log(err);
        return -1;
    }
}

function getListWork(req, res) {
    var username = getUser(req, res);
    //console.log(username);
    knex('work')
        .where({ username: username })
        .join('user', 'user.Id', '=', 'work.userId')
        .select('work.Id','work.title', 'work.description')
        .then((row) => {
            //console.log(row)
            res.send(row);
        })
}





app.get('/', (req, res) => {
    //let{username} = req.body;
    getListWork(req, res);
    //res.send("/api/listwork")

});

module.exports = app;