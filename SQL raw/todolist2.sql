drop database todolist2;
create database todolist2;
use todolist2;

create table `user`
(
	`Id` int not null auto_increment,
	`username` nvarchar(255),
    `password` nvarchar(255),
    primary key(Id)
);

insert into `user`(`username`, `password`)
values 
('user1','123'),
('user2','123'),
('user3','123'),
('user4','123'),
('user5','123');

create table `table_task`
(
	`Id` int not null auto_increment,
    `user_id` int not null,
	`title` nvarchar(255),
    primary key(Id),
    foreign key (user_id) REFERENCES `user`(Id)
);

create table `status`
(
	`Id` int not null auto_increment,
	`status` nvarchar(255),
    primary key(Id)
);
create table `task`
(
	`Id` int not null auto_increment,
    `table_task_id` int,
	`title` nvarchar(255),
    `description` text,
    status_id int,
    assigned_user_id int null,
    primary key(Id),
    foreign key (table_task_id) REFERENCES `table_task`(Id),
    foreign key (status_id) REFERENCES `status`(Id)
);

-- create table `assign`
-- (
-- 	`Id` int not null auto_increment,
--     `user_id` int,
-- 	`task_id` int,
--     primary key(Id),
--     foreign key (task_id) REFERENCES `task`(Id),
--     foreign key (user_id) REFERENCES `user`(Id)
-- );

insert into `status`(`status`)
values 
('on going'),
('done'),
('fail');



insert into `table_task`(`user_id`,`title`)
values 
(1,'week 1'),
(2,'week 2'),
(1,'week 3');



insert into `task`(`table_task_id`,`title`,`description`,`status_id`)
values 
(1,'do my home task first','nothing',1),
(1,'do my mom task','nothing',1),
(2,'let me cook!!','nothing',2),
(2,'im comming soon','nothing',2);

insert into `task`( `table_task_id` ,`title`,`description`,`status_id`,`assigned_user_id`)
values 
(3,'do no thing','nothing',1, 3),
(3,'do a task','nothing',2, 3);


-- insert into `table_task`( `title`,`description`)
-- values 
-- ('do no thing','nothing'),
-- ('do a task','nothing');

-- update `task`
-- set `description`='This world is trapped in the well of suffering. What is this suffering due to?  This suffering stems from ignorance of the Self. All suffering in this world is because of ignorance. This ignorance leads to attachment and abhorrence and suffering is experienced as a consequence. Only True Knowledge & Enlightenment Science can cure this suffering. There is no other remedy. True Knowledge & Enlightenment Science insulates you from suffering.'
-- where id<10;


-- insert into `assign`(`user_id`,`task_id`)
-- values 
-- (3,1),
-- (3,2),
-- (4,2);





